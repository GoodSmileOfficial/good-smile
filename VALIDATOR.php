<?php

abstract class ValidateAbstract
{


    const TYPE_STRING = 1;
    const TYPE_ARRAY = 2;
    const TYPE_FILE = 3;


    protected $_type = ValidateAbstract::TYPE_STRING;


    protected $_attr = [
        'required' => true,
    ];


    public abstract function check($data);


    public function getType()
    {
        return $this->_type;
    }


    public function setAttribute(array $attr)
    {
        $this->_attr = array_merge($this->_attr, $attr);

        return $this;
    }
}

/**
 * Валидаторы
 */
class Multiple extends ValidateAbstract
{
    protected $_type = ValidateAbstract::TYPE_ARRAY;

    public function check($data)
    {
//        echo('<pre>');
//        var_dump($data);
//        echo('</pre>');
        if ([] == $data and true === $this->_attr['required'])
            return 'Вы не выбрали данные';

        else if ([] == $data and false === $this->_attr['required'])
            return true;

//        exit();
        foreach ($data as $row) {
            if (is_array($row))
                return '!Передан многомерный массив. Ошибка передачи данных';

            if (!in_array((string)$row, $this->_attr['values']))
                return '!Один из переданных отмеченных вариантов списка не найден в нем';
        }

        return true;
    }
}

class Oon extends ValidateAbstract
{
    public function check($data)
    {
        if ('' == $data and true === $this->_attr['required'])
            return 'Вы не выбрали данные';

        else if ('' == $data and false === $this->_attr['required'])
            return true;

        if ('1' !== $data)
            return 'Вы не выбрали чекбокс';

        return true;
    }
}

class Email extends ValidateAbstract
{
    public function check($data)
    {
        if ('' == $data and true === $this->_attr['required'])
            return 'Вы не выбрали данные';

        else if ('' == $data and false === $this->_attr['required'])
            return true;

        if (!filter_var($data, FILTER_VALIDATE_EMAIL))
            return 'E-mail введено не верно. Проверьте правильность ввода адреса.';

        return true;
    }
}

class Password extends ValidateAbstract
{
    protected $_attr = [];

    public function check($data)
    {
        if ('' == $data and true === $this->_attr['required'])
            return 'Вы не выбрали данные';

        else if ('' == $data and false === $this->_attr['required'])
            return true;

        if (ctype_digit($data))
            return 'Пароли с одних чисел запрещены';

        if (mb_strlen($data) < 5)
            return 'Пароли должен быть болше 5 символов';

        return true;
    }
}

class Date extends ValidateAbstract
{
    public function check($data)
    {
        if ('' == $data and true === $this->_attr['required'])
            return 'Вы не выбрали данные';

        else if ('' == $data and false === $this->_attr['required'])
            return true;

        if (!preg_match('#^\d{4}-\d{2}-\d{2}$#ui', $data))
            return 'Дата имеет не правильный формат. Передайте данные в формате YYYY-mm-dd.';


        list($year, $month, $day) = explode('-', $data);

        if (false === checkdate($month, $day, $year))
            return 'Дата не существует.';

        return true;
    }
}


class IntValid extends ValidateAbstract
{
    protected $_attr = [
        'required' => true,
        'min' => -2147483648,
        'max' => 2147483647,
    ];

    public function check($data)
    {
        if ($this->_attr['required'] === false and $data === '')
            return true;

        if ($this->_attr['required'] === true and $data === '')
            return 'Вы не передали данные';

        if ($data < $this->_attr['min'])
            return "Ваш текст должен быть минимум {$this -> _attr['min']} символов";

        if ($data > $this->_attr['max'])
            return "Ваш текст должен быть максимум {$this -> _attr['max']} символов";

        return true;
    }
}


class Text extends ValidateAbstract
{
    protected $_attr = [
        'required' => true,
        'minlength' => 1,
        'textarea' => false,
        'maxlength' => 65000,
    ];

    public function check($data)
    {
        if ($this->_attr['required'] === false and $data === '')
            return true;

        if ($this->_attr['required'] === true and $data === '')
            return 'Вы не передали данные';

        if (false === $this->_attr['textarea'] and false !== strpos($data, "\n"))
            return 'В Вашем тексте обнаружены переносы строк';

        $len = mb_strlen($data);

        if ($this->_attr['minlength'] > $len)
            return "Ваш текст должен быть минимум {$this->_attr['minlength']} символов";

        if ($this->_attr['maxlength'] < $len)
            return "Ваш текст должен быть максимум {$this->_attr['maxlength']} символов";

        return true;
    }
}

class Enum extends ValidateAbstract
{
    protected $_attr = [
        'required' => true,
        'values' => [],
    ];

    public function check($data)
    {
        if ('' == $data and true === $this->_attr['required'])
            return 'Вы не выбрали данные';

        else if ('' == $data and false === $this->_attr['required'])
            return true;

        if ([] === $this->_attr['values'])
            return 'Ошибка разработки. Возможные значения не определены';

        if (is_array(each($this->_attr['values'])[1])) {
            $st = false;

            array_walk($this->_attr['values'], function ($value, $key) use ($data, &$st) {
                if (in_array((string)$data, $value))
                    $st = true;
            });

            return $st;
        }

        if (!in_array((string)$data, $this->_attr['values']))
            return 'Переданный вариант списка не найден в нем.';

        return true;
    }
}

class Aids extends ValidateAbstract
{
//  protected $_type = ValidateAbstract::TYPE_FILE;
    protected $_type = ValidateAbstract::TYPE_ARRAY;

    public function check($data)
    {
        foreach ($data as $value) {
            if (false === ctype_digit($value))
                return 'Вы передали массив с текстом';
        }

        return true;
    }
}

class Id extends ValidateAbstract
{
    public function check($data)
    {
        if (!$data)
            return 'Вы не передали ID';

        if (!ctype_digit($data))
            return 'Вы передали ID который не есть числом';

        if ($data > 4294967295)
            return 'ID слишком большой';

        return true;
    }
}

class Name extends ValidateAbstract
{
    public function check($data)
    {
        if (!$data)
            return 'Вы не передали имя';

        if (mb_strlen($data) > 20)
            return 'Имя не должно быть больше 20 символов';

        return true;
    }
}


class Field
{

    protected $_name;

    protected $_validate;


    protected $_data;

    protected $_error;


    const METHOD_POST = 1;
    const METHOD_GET = 2;
    const METHOD_COOKIE = 3;
    const METHOD_REQUEST = 4;

    public final function __construct($name, $validate, array $attr = [])
    {
        $this->_name = $name;

        $this->_validate = new $validate;
        $this->_validate->setAttribute($attr);
    }

    public function getData()
    {
        return $this->_data;
    }

    public function getError()
    {
        return $this->_error;
    }

    public function getName()
    {
        return $this->_name;
    }


    public function validate($method = Field::METHOD_GET)
    {

//        print_r($method);
        $this->getDataMethod($method);


        $status = $this->_validate->check($this->_data);

        if (is_string($status)) {
            $this->_error = $status;
            return false;
        } else
            return true;
    }

    protected function getAllDataFromMethod($method)
    {
//        print_r($method);
//        exit();
        switch ($method) {
            case Field::METHOD_POST :
                return $_POST;
            case Field::METHOD_GET :
                return $_GET;
            case Field::METHOD_COOKIE :
                return $_COOKIE;
            case Field::METHOD_REQUEST :
                return $_REQUEST;
            default :
                return [];
        }
    }


    protected function getDataMethod($method)
    {

        $type = $this->_validate->getType();


        $data = $this->getAllDataFromMethod($method);


        switch ($type) {
            case ValidateAbstract::TYPE_STRING:
                $this->_data = (isset($data[$this->_name]) and is_string($data[$this->_name]))
                    ? trim($data[$this->_name]) : '';
                break;
            case ValidateAbstract::TYPE_ARRAY:
                $this->_data = (isset($data[$this->_name]) and is_array($data[$this->_name]))
                    ? $data[$this->_name] : [];
                break;
            case ValidateAbstract::TYPE_FILE:
                $this->_data = (isset($data[$this->_name])) ? $data[$this->_name] : [];
                break;
            default :
                exit('Error! Undefined type');
        }
    }
}


class Group
{

    protected $_fields = [];


    public function __construct(array $a_fields)
    {

        foreach ($a_fields as $values) {
            $attr = (isset($values[2]) ? $values[2] : []);
            $this->_fields[$values[0]] = new Field($values[0], $values[1], $attr);
        }
    }


    public function getAllData()
    {
        $a_data = [];

        foreach ($this->_fields as $field)
            $a_data[$field->getName()] = $field->getData();

        return $a_data;
    }

    public function getAllError()
    {
        $a_error = [];

        foreach ($this->_fields as $field) {
            if (is_string($field->getError()))
                $a_error[$field->getName()] = $field->getError();
        }

        return $a_error;
    }


    public function isValid($method = Field::METHOD_POST)
    {

        $valid = true;

        foreach ($this->_fields as $field) {
            if (false === $field->validate($method))
                $valid = false;
        }

        return $valid;
    }

    public function getFieldData($name)
    {
        return $this->_fields[$name]->getData();
    }

    public function getFieldError($name)
    {
        return $this->_fields[$name]->getError();
    }
}

//example usage
if (isset($_POST['submit_test'])) {
    $group = new Group([
        ['text', 'text', ['maxlength' => 40,'minlength'=>2]],
        ['password', 'password'],
        ['date', 'date'],
        ['radio', 'enum', ['values' => [1, 2]]],
        ['checkbox', 'oon'],
        ['select', 'enum', ['values' => ['v1', 'v2', 'v3']]],
        ['selectm', 'multiple', ['values' => ['v11', 'v12', 'v13']]],
        ['textarea', 'text', ['textarea' => true]],
    ]);

    //4
    if ($group->isValid()) {
        echo 'Виконуємо дію';
        echo '<pre>';
        print_r($group->getAllData());
        echo '</pre>';
    } else {
        print_r($group->getAllData());

        $a_data = $group->getAllData();
        $a_error = $group->getAllError();

        echo '<h1>В даних знайдено помилки:</h1>';

//        foreach ( $a_error as $key => $error )
//            echo "В полі {$key} знайдено помилку: {$error}<br />";
    }
}
?>
<form action="" method="POST">
    <input type="text" name="text"
           value="<?= (isset($a_data['text']) ? htmlspecialchars($a_data['text']) : '') ?>"/><br/>
    <?php //         value="<?= ( $group -> getFieldData( 'text' ) ? htmlspecialchars( $group -> getFieldData( 'text' ) ) : '' ) " /><br /> ?>

    <?= ((isset($a_error['text'])) ? '<span style="color: red">' . $a_error['text'] . '</span><br />' : '') ?>
    <?= ''//( ( $group -> getFieldError( 'text' ) ) ? '<span style="color: red">' . $group -> getFieldError( 'text' ) . '</span><br />' : '' )   ?>

    <input type="password" name="password"/><br/>
    <?= ((isset($a_error['password'])) ? '<span style="color: red">' . $a_error['password'] . '</span><br />' : '') ?>


    <input required="required" type="text" name="date"
           value="<?= (isset($a_data['date']) ? htmlspecialchars($a_data['date']) : '') ?>"/><br/>
    <?= ((isset($a_error['date'])) ? '<span style="color: red">' . $a_error['date'] . '</span><br />' : '') ?>

    <input type="radio" name="radio"
           value="1" <?= ((isset($a_data['radio']) and $a_data['radio'] == 1) ? ' checked' : '') ?>/><br/>
    <input type="radio" name="radio"
           value="2" <?= ((isset($a_data['radio']) and $a_data['radio'] == 2) ? ' checked' : '') ?>/><br/>
    <?= ((isset($a_error['radio'])) ? '<span style="color: red">' . $a_error['radio'] . '</span><br />' : '') ?>


    <input type="checkbox" name="checkbox"
           value="1"<?= ((isset($a_data['checkbox']) and $a_data['checkbox'] == 1) ? ' checked="checked"' : '') ?>/><br/>
    <?= ((isset($a_error['checkbox'])) ? '<span style="color: #ff2800">' . $a_error['checkbox'] . '</span><br />' : '') ?>

    <select name="select">
        <option></option>
        <option value="v1"<?= ((isset($a_data['select']) and $a_data['select'] == 'v1') ? ' selected' : '') ?>>
            v1
        </option>
        <option value="v2"<?= ((isset($a_data['select']) and $a_data['select'] == 'v2') ? ' selected' : '') ?>>
            v2
        </option>
        <option value="v3"<?= ((isset($a_data['select']) and $a_data['select'] == 'v3') ? ' selected' : '') ?>>
            v3
        </option>
    </select><br/>
    <?= ((isset($a_error['select'])) ? '<span style="color: red">' . $a_error['select'] . '</span><br />' : '') ?>

    <select name="selectm[]" multiple="multiple">
        <option value="v11"<?= ((isset($a_data['selectm']) and in_array('v11', $a_data['selectm'])) ? ' selected' : '') ?>>
            v11
        </option>
        <option value="v12"<?= ((isset($a_data['selectm']) and in_array('v12', $a_data['selectm'])) ? ' selected' : '') ?>>
            v12
        </option>
        <option value="v13"<?= ((isset($a_data['selectm']) and in_array('v13', $a_data['selectm'])) ? ' selected' : '') ?>>
            v13
        </option>
    </select><br/>
    <?= ((isset($a_error['selectm'])) ? '<span style="color: red">' . $a_error['selectm'] . '</span><br />' : '') ?>

    <textarea name="textarea"><?= (isset($a_data['textarea']) ? $a_data['textarea'] : '') ?></textarea><br/>
    <?= ((isset($a_error['textarea'])) ? '<span style="color: red">' . $a_error['textarea'] . '</span><br />' : '') ?>
    <input type="submit" name="submit_test" value="SEND"/>
</form>

